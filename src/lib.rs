use gapp::{Render, Update};
#[cfg(not(target_arch = "wasm32"))]
use glutin::{
    context::PossiblyCurrentContext,
    prelude::*,
    surface::{Surface, WindowSurface},
};
use instant::Instant;
use std::time;
use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Window,
};

pub trait WindowInput<C, R> {
    fn input(
        &mut self,
        event: &WindowEvent,
        control_flow: &mut ControlFlow,
        context: &mut C,
        renderer: &mut R,
    );
}

pub fn run<I: 'static, R: 'static, T, E: WindowInput<I, R> + Render<R> + Update + 'static>(
    mut application: E,
    event_loop: EventLoop<T>,
    fps: u64,
    #[cfg(not(target_arch = "wasm32"))] context: PossiblyCurrentContext,
    #[cfg(not(target_arch = "wasm32"))] surface: Surface<WindowSurface>,
    window: Window,
    mut input_context: I,
    mut renderer: R,
) {
    let timestep = time::Duration::from_nanos(1000000000 / fps);

    let start = Instant::now();
    let mut prev_time = start;

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent { ref event, .. } => {
            #[allow(unused_variables)]
            if let WindowEvent::Resized(physical_size) = event {
                #[cfg(not(target_arch = "wasm32"))]
                surface.resize(
                    &context,
                    physical_size.width.try_into().unwrap(),
                    physical_size.height.try_into().unwrap(),
                );
            }

            application.input(event, control_flow, &mut input_context, &mut renderer);
        }
        Event::RedrawRequested(_) => {
            application.render(&mut renderer);

            #[cfg(not(target_arch = "wasm32"))]
            surface.swap_buffers(&context).unwrap();
        }
        Event::MainEventsCleared => {
            #[allow(unused_variables)]
            let frame_duration = prev_time.elapsed();

            application.update(timestep.as_secs_f32());

            #[cfg(not(target_arch = "wasm32"))]
            if frame_duration < timestep {
                std::thread::sleep(timestep - frame_duration);
            }

            window.request_redraw();

            prev_time = Instant::now();
        }
        _ => (),
    });
}
